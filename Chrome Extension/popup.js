// popup.js
document.addEventListener('DOMContentLoaded', function() {
    var summarizeButton = document.getElementById('summarize');
    summarizeButton.addEventListener('click', function() {
      
      document.getElementById('loading').style.display = 'block';

      chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.scripting.executeScript({
          target: {tabId: tabs[0].id},
          function: () => document.body.innerText
        }, (injectionResults) => {
          if (chrome.runtime.lastError) {
            console.error(chrome.runtime.lastError.message);
            return;
          }
          const [result] = injectionResults;
          const text = result.result;
  
          chrome.runtime.sendMessage({action: "summarizeText", text: text}, response => {
            if (chrome.runtime.lastError) {
              console.error(chrome.runtime.lastError.message);
              document.getElementById('loading').style.display = 'none';              
            } else if (response) {
              document.getElementById('summary').textContent = response.summary;
              document.getElementById('loading').style.display = 'none';
            }
          });
        });
      });
    });
  });
  