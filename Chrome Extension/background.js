// background.js

// This function fetches the summary from OpenAI.
function fetchSummary(text, callback) {
  chrome.storage.local.get('openai_api_key', function(result) {
    if (result.openai_api_key) {
      const apiKey = result.openai_api_key;

      const API_URL = 'https://api.openai.com/v1/chat/completions';

      fetch(API_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${apiKey}`
        },
        body: JSON.stringify({
          model: "gpt-3.5-turbo",
          messages: [{ "role": "system", "content": "Summarize the important parts of this privacy policy in 2-3 sentences and tell me if there is anythign suspisious I need to be worried about:" }, { "role": "user", "content": text }]
        }),
      })
      .then(response => {
        if (!response.ok) {
          // Extracting the text to check the detailed error message
          return response.text().then(text => Promise.reject(new Error(text)));
        }
        return response.json();
      })
      .then(data => {
        if (data.choices && data.choices.length > 0 && data.choices[0].message && data.choices[0].message.content) {
          callback(null, data.choices[0].message.content); // Pass the summary back to the popup.
        } else {
          throw new Error('Invalid response structure from OpenAI API');
        }
      })
      .catch(error => {
        console.error('Error during the API call:', error);
        callback(new Error('An error occurred while summarizing the text.')); // Pass the error back to the popup.
      });
    } else {
      console.error('No API key found in storage.');
      callback(new Error('No API key found. Please set the API key via the options page.'));
    }
  });
}

// Listening for messages from the popup.
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.action === "summarizeText") {
    fetchSummary(request.text, function(err, summary) {
      if (err) {
        sendResponse({error: err.message});
      } else {
        sendResponse({summary: summary});
      }
    });
    return true; // indicates that you wish to send a response asynchronously
  }
});
