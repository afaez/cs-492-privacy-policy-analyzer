// Save the API key to chrome.storage.local
function saveOptions() {
    var apiKey = document.getElementById('api_key').value;
    chrome.storage.local.set({'openai_api_key': apiKey}, function() {
      // Update status to let user know options were saved.
      console.log('Settings saved');
    });
  }
  
  // Restore the API key state from chrome.storage.local
  function restoreOptions() {
    chrome.storage.local.get('openai_api_key', function(data) {
      document.getElementById('api_key').value = data.openai_api_key || '';
    });
  }
  
  // Event listeners for save button
  document.addEventListener('DOMContentLoaded', restoreOptions);
  document.getElementById('save').addEventListener('click', saveOptions);
  