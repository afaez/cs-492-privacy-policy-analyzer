// content.js

function checkForPrivacyPolicy() {
  console.log('Content script has loaded, checking for privacy policy...');
  let bodyText = document.body.innerText || "";
  let found = bodyText.toLowerCase().includes("privacy"); // Case-insensitive check
  if(found) {
    console.log('Privacy policy detected.');
    alert('Privacy policy detected on this page. Consider using the PolicyPatrol extension to summarize the policy.');
  } else {
    console.log('No privacy policy detected.');
  }
}

// Run the check when the page has loaded
document.addEventListener("DOMContentLoaded", checkForPrivacyPolicy);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.action === "checkForPrivacy") {
      let bodyText = document.body.innerText || "";
      let found = bodyText.includes("privacy");
      console.log('Message received to check for privacy: ', found);
      sendResponse({found: found});
    }
  }
);
checkForPrivacyPolicy();
