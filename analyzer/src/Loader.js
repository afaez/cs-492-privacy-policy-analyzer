import SyncLoader from "react-spinners/SyncLoader";

export const Loader = ({withMargin = false}) => {
    return (
        <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column', gap: '2rem', margin: withMargin ? '2rem 0 2rem 0' : '0'}}>
            <SyncLoader color="white" size={20} aria-label="Loading Spinner"/>
            <div>Loading</div>
        </div>
    )
}