import "./App.css";
import { Loader } from "./Loader";
import { getHighlightDetails, getHighlights, getSummary } from "./requests";
import { useState } from "react";
import logoImg from './img/logo.png';
import level1Img from './img/level1.png';
import level2Img from './img/level2.png';
import level3Img from './img/level3.png';
import { Content } from "./Content";

function App() {
  const [summary, setSummary] = useState("");
  const [highlights, setHighlights] = useState("");
  const [details, setDetails] = useState("");
  const [clicked, setClicked] = useState(false);
  const [inputText, setInputText] = useState("");
  const [isEmptyInput, setIsEmptyInput] = useState(false);
  const [isLoadingDetails, setIsDetailsLoading] = useState(false);
  const [file, setFile] = useState(null);
  const [level1, setLevel1] = useState(0);
  const [level2, setLevel2] = useState(0);
  const [level3, setLevel3] = useState(0);

  const getHighlightColour = (level) => {
    switch (parseInt(level)) {
      case 1:
        return "rgba(224, 199, 3, 255)";
      case 2:
        return "rgba(249, 130, 0, 255)";
      case 3:
        return "rgba(248, 37, 0, 255)";
      default:
        return "transparent";
    }
  };

  const parseHighlightedStatement = (statement) => {
    const regex = /\[CS492-UID-START-(\d+)\]([\s\S]*?)\[CS492-UID-END-\d+\]/g;
    let parsedString = [];
    let lastIndex = 0;
    let highlighted;

    while ((highlighted = regex.exec(statement)) !== null) {
      const [fullStatement, level, content] = highlighted;
      const index = highlighted.index;
      console.log("check", typeof level)
      if (level == 1) {
        console.log("it's in level 1")
        setLevel1(level1 + 1);
      } else if (level == 2) {
        console.log("it's in level 2")
        setLevel2(level2 + 1);
      } else if (level == 3) {
        console.log("it's in level 3")
        setLevel3(level3 + 1);
      }

      // Text before highlight
      parsedString.push(statement.substring(lastIndex, index));

      // Highlighted statement
      parsedString.push(
        <span
          key={index}
          style={{
            fontWeight: "bold",
            backgroundColor: getHighlightColour(level),
            cursor: "pointer",
          }}
          onClick={async () => {
            setDetails('');
            setIsDetailsLoading(true);
            const response = await getHighlightDetails(fullStatement);
            const regex2 = /<div id="details">([\s\S]*?)<\/div>/;
            const match = response?.match(regex2);
            const filteredResponse = match ? match[1] : "";
            setDetails(filteredResponse);
            setIsDetailsLoading(false);
          }}
        >
          {content}
        </span>
      );

      lastIndex = index + fullStatement.length;
    }

    parsedString.push(statement.substring(lastIndex));
    return parsedString;
  };

  const handleFile = (event) => {
    setIsEmptyInput(false)
    setFile(event.target.files[0])
  };

  return (
    <div className="App">
      <div
        className="App-header"
        style={{ display: "flex", flexDirection: "column", gap: "1rem" }}
      >
        <div>
          <h1>PolicyPatrol</h1>
          <h3 style={{marginTop: '-1rem'}}>Privacy Policy Analyzer</h3>
          <img alt="image of logo" src={logoImg} style={{width: '25%', border: '2px gray solid', borderRadius: '0.5rem'}}/>
        </div>
        {!clicked && (
          <>
            <Content/>
            <div style={{color:'orange', fontSize: '1.5rem', marginBottom: '0.2rem'}}>If you attach a file and paste text, only the file will be analyzed.</div>
            <div style={{color:'orange', fontSize: '1.5rem', marginBottom: '1rem'}}>File upload feature only supports single page PDFs.</div>
            <textarea
              value={inputText}
              onChange={(e) => {
                setIsEmptyInput(false);
                setInputText(e.target.value);
              }}
              rows={10}
              cols={50}
              style={{border: '2px gray solid', borderRadius: '0.5rem', padding: '0.5rem'}}
              placeholder="Insert the privacy policy text here ..."
            />
            <br />
            <div>
              <input
                type="file"
                name="file"
                style={{
                  marginTop: "1rem",
                  padding: '0.5rem',
                  border: "solid 2px gray",
                  fontSize: "1.5rem",
                  cursor: "pointer",
                  borderRadius: '0.5rem',
                  textAlign: 'center'
                }}
                onChange={handleFile}
              />
            </div>
            {isEmptyInput && <div style={{fontWeight: 'bold', color: 'red', margin: '1rem 0 1rem 0', fontSize: '1.4rem'}}>Please paste a policy or attach a file!</div>}
            <button
              style={{
                marginTop: "1rem",
                padding: "10px",
                border: "solid 2px gray",
                fontSize: "2rem",
                cursor: "pointer",
                borderRadius: "10px",
              }}
              onClick={async () => {
                if (!inputText.trim().length && !file) {
                    setIsEmptyInput(true);
                    return;
                }
                
                setClicked(true);
                let bundle = {}

                if (file) {
                  bundle.policyFile = file
                } else {
                  bundle.policyText = inputText
                }

                const response1 = await getSummary(bundle);
                const regex1 = /<div id="summary">([\s\S]*?)<\/div>/;
                const match1 = response1?.match(regex1);
                const filteredResponse1 = match1 ? match1[1] : "";
                setSummary(filteredResponse1);

                const response2 = await getHighlights(bundle);
                const regex2 = /<div id="highlights">([\s\S]*?)<\/div>/;
                const match2 = response2?.match(regex2);
                const filteredResponse2 = match2 ? match2[1] : "";
                const parsed = parseHighlightedStatement(filteredResponse2);
                setHighlights(parsed);
              }}
            >
              Analyze Policy
            </button>
          </>
        )}
        {clicked && !summary && !highlights && !details && (
          <Loader withMargin/>
        )}
        {summary.length > 0 && (
          <div
            style={{
              border: "solid 2px gray",
              padding: "1rem",
              margin: "1rem 4rem 0 4rem",
              borderRadius: "10px",
            }}
          >
            <div style={{ fontWeight: "bold", marginBottom: "1rem" }}>
              Summary
            </div>
            {summary}
          </div>
        )}
        {summary.length > 0 && highlights.length == 0 && <Loader withMargin/>}
        {highlights.length > 0 && (
          <div
            style={{
              border: "solid 2px gray",
              padding: "1rem",
              margin: "0 4rem 0 4rem",
              borderRadius: "10px",
            }}
          >
            <div style={{ fontWeight: "bold", marginBottom: "1rem" }}>
              Highlighted Policy
            </div>
            {(level1 + level2 + level3 === 0) ?
              <div style={{margin: '0 0 2rem 0', color: 'lightgreen'}}>No Concerning Statements Found</div> :
              <div style={{marginBottom: '2rem', display: 'flex', gap: '4rem', justifyContent: 'center'}}>
                <div style={{display: 'flex', alignItems: 'center', gap: '0.8rem'}}><img alt="image of level 1" src={level1Img} style={{width: '2rem'}}/>{level1}</div>
                <div style={{display: 'flex', alignItems: 'center', gap: '0.8rem'}}><img alt="image of level 2" src={level2Img} style={{width: '2rem'}}/>{level2}</div>
                <div style={{display: 'flex', alignItems: 'center', gap: '0.8rem'}}><img alt="image of level 3" src={level3Img} style={{width: '2rem'}}/>{level3}</div>
              </div>
            }
            {highlights}
          </div>
        )}
        {isLoadingDetails && <Loader withMargin/>}
        {details.length > 0 && (
          <div
            style={{
              border: "solid 2px gray",
              padding: "1rem",
              margin: "0 4rem 2rem 4rem",
              borderRadius: "10px",
            }}
          >
            <div style={{ fontWeight: "bold", marginBottom: "1rem" }}>
              Details of Highlighted Statement
            </div>
            {details}
          </div>
        )}
        {clicked && (
          <button
            style={{
              marginTop: "1rem",
              padding: "10px",
              border: "solid 2px gray",
              fontSize: "2rem",
              cursor: "pointer",
              borderRadius: "10px",
            }}
            onClick={() => {
              setClicked(false);
              setLevel1(0);
              setLevel2(0);
              setLevel3(0);
              setInputText("");
              setFile(null);
              setSummary("");
              setHighlights("");
              setDetails("");
            }}
          >
            Back
          </button>
        )}
      </div>
    </div>
  );
}

export default App;
