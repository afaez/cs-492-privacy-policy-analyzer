export const ListContent = ({title, list, img = null, refer = null}) => {
    return (
        <div style={{display: 'flex', gap: '1rem', alignItems: 'center', justifyContent: 'space-between'}}>
            <div>
                <div style={{fontWeight: 'bold', marginBottom: '2rem'}}>{title}</div>
                {list.map(str => <div style={{margin: '1rem'}}>• {str}</div>)}
            </div>
            {img && 
                <div>
                    <img alt="image associated with list" src={img} style={{width: '28rem', height: '25rem', border: '2px gray solid', borderRadius: '1rem'}}/>
                    {refer && <div style={{fontSize: '0.9rem', width: '28rem'}}>{refer}</div>}
                </div>
            }
            
        </div>
    )
}