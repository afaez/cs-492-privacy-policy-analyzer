import { ListContent } from './ListContent'
import stat1 from './img/stat1.png'
import stat2 from './img/stat2.png'

const bullet = require ('string.bullet')


export const Content = () => {
    return (
        <div style={{padding: '5rem', display: 'flex', flexDirection: 'column', gap: '5rem'}}>
            <div>
                Discover PolicyPatrol - your ultimate companion in navigating the intricate world of online privacy policies effortlessly.
                Harnessing the latest in AI technology, PolicyPatrol empowers users to decode complex policies with ease, ensuring you stay informed and in control of your digital footprint.
                With our intuitive platform, uncover hidden truths, identify potential risks, and access concise summaries in seconds.
                Whether you're a savvy online shopper, a busy professional, or a concerned citizen, PolicyPatrol is your go-to solution for safeguarding your privacy online.
                Join the thousands who trust PolicyPatrol to navigate the digital landscape with confidence.
                Take charge of your privacy journey today with PolicyPatrol - where knowledge meets empowerment.
            </div>
            <div style={{textAlign: 'left'}}>
                <ListContent
                title={`What's The Issue?`}
                list={[
                    'Privacy policies are often lengthy and obscure with the main aim of confusing users into agreeing into accepting all terms and conditions.',
                    'Users struggle to understand data handling practices.',
                    'Legal jargon and technical terms complicate comprehension.',
                    'Companies may exploit vague terms to exploit user data.',
                    'Lack of transparency leads to uncertainty and mistrust.',
                    'Users may unknowingly agree to terms compromising privacy.'
                ]}
                img={stat1}
                refer={`Cakebread, C. (2017, November 15). You're not alone, no one reads terms of service agreements. Business Insider. https://www.businessinsider.com/deloitte-study-91-percent-agree-terms-of-service-without-reading-2017-11`}
                />
            </div>
            <div style={{textAlign: 'left'}}>
                <ListContent
                title={`Why Have Previous Attempts Failed?`}
                list={[
                    'Inadequate handling of the complexity within privacy policies, leading to insufficient comprehension.',
                    'Lack of robust tools capable of interpreting nuanced language and legal terminology.',
                    'Limited scalability of manual reading and basic keyword matching approaches.',
                    'Inability to accommodate the diverse range of policy formats used by different companies.',
                    'Failure to keep pace with the rapidly evolving technology and regulatory environment.'
                ]}
                img={stat2}
                refer={`Muhammad, Z. (2021, December 29). 97 percent of adults don't read terms and conditions of online platforms, here's why. Digital Information World. https://www.digitalinformationworld.com/2021/12/97-percent-of-adults-dont-read-terms.html`}
                />
            </div>
        </div>  
    )
}