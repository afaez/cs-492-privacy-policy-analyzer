
export const getSummary = async (bundle) => {
    try {
      const formData = new FormData();
      if ('policyText' in bundle) {
        formData.append('policyText', bundle.policyText);
      } else if ('policyFile' in bundle) {
        formData.append('policyFile', bundle.policyFile);
      }
  
      const response = await fetch('http://127.0.0.1:5000/upload', {
        method: 'POST',
        body: formData
      });
  
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
  
      const data = await response.text();

      return data;
    } catch (error) {
      console.error('Error fetching highlights:', error);
    }
};

export const getHighlights = async (bundle) => {
    try {
      const formData = new FormData();
      if ('policyText' in bundle) {
        formData.append('policyText', bundle.policyText);
      } else if ('policyFile' in bundle) {
        formData.append('policyFile', bundle.policyFile);
      }
  
      const response = await fetch('http://127.0.0.1:5000/highlights', {
        method: 'POST',
        body: formData
      });
  
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
  
      const data = await response.text();
      
      return data;
    } catch (error) {
      console.error('Error fetching highlights:', error);
    }
};

export const getHighlightDetails = async (policyText) => {
    try {
      const formData = new FormData();
      formData.append('policyText', policyText);

      const response = await fetch('http://127.0.0.1:5000/highlight-details', {
        method: 'POST',
        body: formData
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.text();

      return data;
    } catch (error) {
      console.error('Error fetching highlights:', error);
    }
};