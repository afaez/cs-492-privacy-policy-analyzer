from flask import Flask, request, render_template, send_file
from PyPDF2 import PdfReader
from flask_cors import CORS
import openai
import os

app = Flask(__name__)
CORS(app)
openai.api_key = os.getenv('OPENAI_API_KEY')

gpt_model = "gpt-3.5-turbo-0125" # replace the text with "gpt-4" to easily switch to gpt-4, but it costs a LOT MORE MONEY.
mx_tkn = 4000

@app.route('/style.css')
def serve_css():
    return send_file('style.css')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload():    
    summarized_text = ""
    text_data= ""
    file = request.files.get('policyFile')
    if file and file.filename != '':
        reader = PdfReader(file)
        text_data = reader.pages[0].extract_text()
    elif 'policyText' in request.form:
        text_data = request.form['policyText']
    
    print("Data Received:", text_data)

    if text_data.strip():  # Check if text_data is not empty
        response = openai.ChatCompletion.create(
            model=gpt_model,
            messages=[
                {
                    "role": "system",
                    "content": "You are a helpful assistant. Summarize this privacy policy in simple English. Never use bullet points or lists."
                },
                {
                    "role": "user",
                    "content": text_data
                }
            ],
            temperature=0.7,
            max_tokens=mx_tkn,
            top_p=1
        )
        if response['choices']:
            summarized_text = response['choices'][0]['message']['content']
            print("Summary:", summarized_text)
        else:
            summarized_text = "No summary could be generated."

    return render_template('summary.html', summarized_text=summarized_text)

@app.route('/highlights', methods=['POST'])
def getHighlights():
    highlighted_text = ""
    text_data = ""
    file = request.files.get('policyFile')

    if file and file.filename != '':
        # Read the content of the file
        reader = PdfReader(file)
        text_data = reader.pages[0].extract_text()
    elif 'policyText' in request.form:
        text_data = request.form['policyText']

    print("Data Received:", text_data)

    if text_data.strip():  # Check if text_data is not empty
        response = openai.ChatCompletion.create(
            model=gpt_model,
            messages=[
                {
                    "role": "system",
                    "content": "You are a helpful assistant tasked with analyzing privacy policies for statements that may raise concerns regarding user privacy and data protection. Your objective is to identify sentences within the privacy policy text that could potentially infringe upon the best interests of the user or product consumer. When you encounter such sentences, mark them by adding[CS492-UID-START-[number here]] at the beginning and[CS492-UID-END-[same number here]] at the end of each individual sentence without adding any extra spaces. However, only apply these markers to statements that are particularly concerning, such as those indicating the sharing or selling of user data, lack of security guarantees, or disclosures to comply with legal obligations. Use numbers 1 to 3 to denote the level of concern for each pair. Assign 1 to sentences that are least concerning, 2 to sentences that are more concerning, and 3 to sentences that are the most concerning. Ensure that each[CS492-UID-START-[number here]]and[CS492-UID-END-[same number here]]pair strictly surrounds exactly one sentence, not multiple sentences combined. A sentence is complete when it ends with a full stop. Ensure that all other sentences remain unchanged. Make sure to strictly always repeat every sentence in the response with no words changed except for adding the pair - every single sentence. Here is an example of how to mark concerning statements: Example: Input: At XYZ Fitness Tracker, we collect personal information like name, email, and location to enhance your experience. Your data may be shared with third-party partners for marketing purposes. While we strive to protect your information, we cannot guarantee absolute security. We reserve the right to disclose your information to comply with legal obligations. Users should be cautious about sharing sensitive data. Output: At XYZ Fitness Tracker, we collect personal information like name, email, and location to enhance your experience. [CS492-UID-START-1]Your data may be shared with third-party partners for marketing purposes.[CS492-UID-END-1]While we strive to protect your information, we cannot guarantee absolute security. [CS492-UID-START-2]We reserve the right to disclose your information to comply with legal obligations.[CS492-UID-END-2] Users should be cautious about sharing sensitive data. Please apply this process to analyze privacy policy texts and identify concerning statements accordingly."
                },
                {
                    "role": "user",
                    "content": text_data
                }
            ],
            temperature=0.7,
            max_tokens=mx_tkn,
            top_p=1
        )
        if response['choices']:
            highlighted_text = response['choices'][0]['message']['content']
            print("Highlights:", highlighted_text)
        else:
            highlighted_text = "No highlights could be generated."

    return render_template('highlights.html', highlighted_text=highlighted_text)

@app.route('/highlight-details', methods=['POST'])
def getHighlightDetails():
    # Handle the text area input
    text_data = request.form['policyText']
    print("Data received:", text_data)
    
    details_text = ""
    
    if 'policyText' in request.form:
        text_data = request.form['policyText']
        if text_data.strip():  # Check if text_data is not empty
            response = openai.ChatCompletion.create(
                model=gpt_model,
                messages=[
                    {
                        "role": "system",
                        "content": "As an agent tasked with analyzing privacy policies, you've previously highlighted a statement provided to you from a privacy policy that you have found to be concerning. Note that some sentences were never given a rating, which means they were safe for users. You must never show support in your response for these statements. You have rated the statement from 1 to 3. 1 to 3 denote the level of concern for each pair. 1 is for sentences that are least concerning, 2 is for sentences that are more concerning, and 3 is for sentences that are most concerning. Please provide detailed reasons explaining why you considered this statement to be concerning. Your answer should be based on the rating provided from 1 to 3. A rating for a statement can be identified by looking at [CS492-UID-START-[number here]]. Look at [number here], that section will be 1 to 3. Include any relevant factors such as the potential impact on user privacy, data protection, or other concerns outlined in the privacy policy analysis guidelines. Your explanation should provide clear justification for the assigned rating and help ensure accurate evaluation of the statement's implications - mention why it is not a higher rating than the current one provided. Add new lines in your response wherever necessary to make it easier for the user to read your response. Keep it at most 10 lines, excluding new lines. Do not use any type of quotes or apostrophes anywhere in your response. Never use characters that require &#[], such as mentioning &#39. Example: Input: [CS492-UID-START-2] Our company may share your personal information with third-party advertisers for targeted advertising purposes.[CS492-UID-END-2]. Output: The highlighted statement was rated as 2 because it suggests that personal information may be shared with third-party advertisers for targeted advertising purposes. This raises concerns about user privacy and the potential for data misuse. While targeted advertising can enhance user experience, it also poses risks of privacy infringement and data exploitation. Therefore, it's important to carefully consider the implications and ensure transparency and user consent in such data-sharing practices."
                    },
                    {
                        "role": "user",
                        "content": text_data
                    }
                ],
                temperature=0.7,
                max_tokens=mx_tkn,
                top_p=1
            )
            if response['choices']:
                details_text = response['choices'][0]['message']['content']
                print("Highlight Details:", details_text)
            else:
                details_text = "No details could be generated."

    return render_template('details.html', details_text=details_text)

if __name__ == '__main__':
    app.run(debug=True)
